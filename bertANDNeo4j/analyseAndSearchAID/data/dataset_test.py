
# 导入所需的库
import torch
# torch.set_printoptions(threshold=torch.inf)
# import wandb
from torch.utils.data import Dataset, DataLoader
from transformers import BertTokenizerFast, BertForTokenClassification, AdamW
import json
import numpy as np
import random
import os
current_dir = os.path.dirname(os.path.abspath(__file__))

class NERTestDataset(Dataset):
    def __init__(self, path, label2id, nofile=False):
        # super().__init__()
        self.data = None
        if nofile:
            self.data = [path]
        else:
            self.data = self.__getdata(path)
        print("testdataset加载")
        self.tokenizer = BertTokenizerFast.from_pretrained(current_dir+'/../ckiplab/bert-base-chinese-ner')
        self.label2id = label2id
        # self.__getitem__(47)
        # self.__getitem__(97)
        
    def __len__(self):
        # return 64
        return len(self.data)
    
    def __getitem__(self, index):
        text = self.data[index]
        text = self.__fix_text(text)
        encoding = self.tokenizer.encode_plus(
            text,
            max_length=512,
            truncation=True,
            return_offsets_mapping=True,
            padding="max_length",
            return_tensors="pt"
        )
        input_ids = encoding["input_ids"].squeeze()
        attention_mask = encoding["attention_mask"].squeeze()
        length = (attention_mask==1).sum().item()
        offset = encoding['offset_mapping'].squeeze()
        offset = offset[:,0][1:length-1]
        labels = torch.zeros(512, dtype=torch.long)
        # print(self.tokenizer.decode(input_ids))
        return input_ids, attention_mask, length
    
    def __getdata(self, path):
        data = []
        with open(path, "r", encoding="utf-8") as f:
            for line in f:
                data.append(str(line))
        return data
    
    def __fix_text(self, text):
        char_dict = {
            "，": ",", "。": ".", "！": "!", "？": "?", "；": ";", "：": ":",
            "“": "'", "”": "'", "‘": "'", "’": "'", "（": "(", "）": ")",
            "《": "<", "》": ">", "【": "[", "】": "]", "、": ",", "妚":"怀"
        }
        return text.translate(str.maketrans(char_dict))
    
    def my_decoder(token_id):
    # 自定义映射
        if token_id == 100:
            return 'X'