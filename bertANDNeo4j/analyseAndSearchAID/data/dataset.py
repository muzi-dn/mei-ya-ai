
# 导入所需的库
import torch
# torch.set_printoptions(threshold=torch.inf)
# import wandb
from torch.utils.data import Dataset, DataLoader
from transformers import BertTokenizerFast, BertForTokenClassification, AdamW
import json
import numpy as np
import random

class NERDataset(Dataset):
    def __init__(self, path, label2id):
        # super().__init__()
        self.data = self.__getdata(path)
        self.tokenizer = BertTokenizerFast.from_pretrained('ckiplab/bert-base-chinese-ner')
        self.label2id = label2id
        # self.__getitem__(47)
        # self.__getitem__(97)
        
    def __len__(self):
        # return 64
        return len(self.data)
    
    def __getitem__(self, index):
        item = self.data[index]
        text = item['sentText']
        text = self.__fix_text(text)
        # print(text)
        entity = item['entityMentions']
        encoding = self.tokenizer.encode_plus(
            text,
            max_length=512,
            truncation=True,
            return_offsets_mapping=True,
            padding="max_length",
            return_tensors="pt"
        )
        input_ids = encoding["input_ids"].squeeze()
        attention_mask = encoding["attention_mask"].squeeze()
        length = (attention_mask==1).sum().item()
        offset = encoding['offset_mapping'].squeeze()
        offset = offset[:,0][1:length-1]
        labels = torch.zeros(512, dtype=torch.long)
        nonolabel_count = 0
        # print(self.tokenizer.decode(input_ids))
        for e in entity:
            start = e['start']
            end = e['end']
            # print(text[start:end])
            label = e['label']
            start = self.__get_offset(start, offset) + 1
            end = self.__get_offset(end, offset) + 1
            # print(self.tokenizer.decode(input_ids[start:end]))
            # start_token, end_token = self.tokenizer.convert_offsets_to_tokens(input_ids, [(start, end)])[0]
            start_token, end_token = (start if start<512 else 511), (end if end<512 else 511)
            nonolabel_count += (end_token - start_token)
            if start_token == end_token:
                labels[start_token] = self.label2id[f"B-{label}"]
            else:
                # 如果实体对应多个token，那么第一个token赋值B-类型的标签，其余token赋值I-类型的标签
                labels[start_token] = self.label2id[f"B-{label}"]
                labels[start_token+1:end_token] = self.label2id[f"I-{label}"]
        
        # print(len(input_ids), len(attention_mask), len(labels))
        return input_ids, attention_mask, labels, length, nonolabel_count/length
    
    def __getdata(self, path):
        data = []
        with open(path) as f:
            for line in f:
                data.append(json.loads(line))
        return data
    
    def __get_offset(self, n, nums):
        index = np.searchsorted(nums, n)
        return index
    
    def __fix_text(self, text):
        char_dict = {
            "，": ",", "。": ".", "！": "!", "？": "?", "；": ";", "：": ":",
            "“": "'", "”": "'", "‘": "'", "’": "'", "（": "(", "）": ")",
            "《": "<", "》": ">", "【": "[", "】": "]", "、": ",", "妚":"怀"
        }
        return text.translate(str.maketrans(char_dict))
    
    def my_decoder(token_id):
    # 自定义映射
        if token_id == 100:
            return 'X'