
# 导入所需的库
import torch
torch.set_printoptions(threshold=torch.inf)
from torch.utils.data import Dataset, DataLoader
from transformers import BertTokenizerFast, BertForTokenClassification, AdamW
from .data.dataset_test import NERTestDataset
from tqdm import tqdm
import os
current_dir = os.path.dirname(os.path.abspath(__file__))

class MYAIENTITY():
    def __init__(self):
        pass
    
    def run(self, input:str):
        entity_types = ["Nh", "Ns", "NT", "NDR", "NW"]
        labels = ["O"] + [f"B-{t}" for t in entity_types] + [f"I-{t}" for t in entity_types]
        label2id = {l: i for i, l in enumerate(labels)}
        id2label = {i: l for i, l in enumerate(labels)}

        dataset = NERTestDataset(input, label2id=label2id, nofile=True)
        test_dataloader = DataLoader(dataset, batch_size=1, shuffle=True, drop_last=True)
        
        device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        print("加载预训练模型entity")
        model = BertForTokenClassification.from_pretrained(
            current_dir+"/ckiplab/bert-base-chinese-ner", 
            # num_labels=len(labels), 
            # id2label=id2label,
            # ignore_mismatched_sizes=True
        ).to(device)
        print("加载模型entity")
        model.load_state_dict(torch.load(current_dir+"/model/saved_model_16.pt"), strict=False)
        print("加载成功entity")
        
        pbar = tqdm(total=len(dataset))
        res = []
        for t in test_dataloader:
            input_ids, attention_mask , length, *_= t
            input_ids = input_ids.to(device)
            attention_mask = attention_mask.to(device)
            output = model(input_ids, attention_mask).logits
            predictions = torch.argmax(output, dim=-1)
            for i,l in enumerate(length):
                pbar.update(1)
                res_tmp = {}
                i_predictions = predictions[i][1:l-1]
                i_input_ids = input_ids[i][1:l-1]
                # text = dataset.tokenizer.decode(i_input_ids)
                text = dataset.tokenizer.decode(i_input_ids, skip_special_tokens=True, decoder=dataset.my_decoder)
                text = text.replace(" ", "")
                res_tmp["sentText"] = text
                res_tmp["entityMentions"] = []
                # print(f"原文：\n{text}")
                # print(f"实际实体数组:")
                # print(zip(i_input_ids, i_labels))
                entity_name = ""
                entity_type = ""
                # last_type = ""
                for i, (token, pred) in enumerate(zip(i_input_ids.tolist(), i_predictions.tolist())):
                    if dataset.tokenizer.decode(token) == "[UNK]":
                        # print(token)
                        token = 'X'
                    else:
                        token = dataset.tokenizer.decode(token)
                    if pred == 0:
                        if entity_type != "":
                            res_tmp["entityMentions"].append({"text":entity_name, "label": entity_type})
                            entity_name = ""
                            entity_type = ""
                        continue
                    if 1<=pred and pred<=5:
                        if entity_type != "":
                            res_tmp["entityMentions"].append({"text":entity_name, "label": entity_type})
                            entity_name = ""
                            entity_type = ""
                        entity_type = entity_types[pred-1]
                    if 1<=pred and pred<=10:
                        entity_name += str(token)
                res.append(res_tmp)
            # break
        pbar.close()
        # print(res)
        return res
    
    def clear(self):
        torch.cuda.empty_cache()