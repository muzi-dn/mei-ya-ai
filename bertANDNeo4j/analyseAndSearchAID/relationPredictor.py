import torch
from transformers import BertTokenizer, BertForSequenceClassification
import pickle
import os
current_dir = os.path.dirname(os.path.abspath(__file__))

class RelationPredictor:
    def __init__(self,mode_path=current_dir+'/bert-base-chinese',pt_path=current_dir+'/model/saved_model_NA.pt',pkl_path=current_dir+'/model/label_encoder__有NA.pkl'):
        self.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        self.drugs = self.getDrugs()
        
        # self.model = torch.load(pt_path)
        self.model = BertForSequenceClassification.from_pretrained(mode_path, num_labels=5)
        print("加载模型relation")
        print(pt_path)
        self.model.load_state_dict(torch.load(pt_path), strict=False)
        print("加载成功relation")
        self.model.eval()
        self.model.to(self.device)

        with open(pkl_path, 'rb') as f:
            self.label_encoder = pickle.load(f)

        # 创建分词器
        self.tokenizer = BertTokenizer.from_pretrained(mode_path)


    def unique_list(self,entityMentions: list):
        result = []
        for i in entityMentions:
            if i not in result:
                result.append(i)
        return result

    def getDrugs(self):
        with open(current_dir+'/data/train.json', mode='r', encoding='utf-8') as f:
            train_json = f.read().split('\n')
        train_json = [eval(i) for i in train_json]
        drugs = []
        for i in train_json:
            entityMentions = i['entityMentions']
            for entityMention in entityMentions:
                label = entityMention['label']
                if label == 'NDR':
                    drugs.append(entityMention['text'])
        drugs = set(drugs)
        return list(drugs)

    def predict_relation(self,entity1, entity2, sentence):
        # 加载模型和标签编码器

        # 预处理文本
        text = "[CLS] " + entity1 + " [SEP] " + entity2 + " [SEP] " + sentence
        inputs = self.tokenizer(text, padding='max_length', truncation=True, max_length=512, return_tensors='pt')

        # 将输入数据移动到指定设备上
        inputs = {key: val.to(self.device) for key, val in inputs.items()}

        # 进行预测
        with torch.no_grad():
            outputs = self.model(**inputs)
            logits = outputs.logits

            # 使用softmax将概率转换为类别预测
            predicted_labels = torch.argmax(torch.softmax(logits, dim=-1), dim=-1)
            predictions = predicted_labels.cpu().numpy()

        # 使用LabelEncoder对象进行标签解码
        predictions_decoded = self.label_encoder.inverse_transform(predictions)

        return predictions_decoded[0]
    
if __name__ == "__main__":
    model = BertForSequenceClassification.from_pretrained('bert-base-chinese', num_labels=5)
    # model.load_state_dict(torch.load('model/saved_model_NA.pt'), strict=False)
    # model.load_state_dict(torch.load('model/saved_model_NA.pt'))
    torch.load('model/saved_model_NA.pt')