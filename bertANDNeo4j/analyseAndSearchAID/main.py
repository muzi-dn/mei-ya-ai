from .relationPredictor import RelationPredictor
from .EntityRecognizer import MYAIENTITY
import random
from .GraphInterface import Graph_Interface
import pickle
from .neo4jQueryExecutor import Neo4jQueryExecutor
import os
current_dir = os.path.dirname(os.path.abspath(__file__))

class Main:
    def __init__(self):
        self.entityRecognizer = MYAIENTITY()
        self.relation_predictor = RelationPredictor()
        self.drugs = self.getDrugs()
        self.neo4j_query_executor = Neo4jQueryExecutor()
    
    def generate_random_id(self):
        # 随机生成地区码（前6位）
        area_code = f'{random.randint(110000, 659004):06d}'

        # 随机生成出生日期（8位，格式为yyyyMMdd）
        year = random.randint(1950, 2005)
        month = random.randint(1, 12)
        day = random.randint(1, 28)  # 为简化处理，假设每个月都是28天
        birthday = f'{year:04d}{month:02d}{day:02d}'

        # 随机生成顺序码（3位）
        sequence_code = f'{random.randint(1, 999):03d}'

        # 前17位身份证号码
        id_without_checksum = area_code + birthday + sequence_code

        # 计算校验码（最后一位）
        weight_factors = [int(i) for i in '7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2'.split(', ')]
        checksum_values = [int(id_without_checksum[i]) * weight_factors[i] for i in range(17)]
        checksum = (12 - sum(checksum_values) % 11) % 11
        checksum = 'X' if checksum == 10 else str(checksum)

        # 最终身份证号码
        id_number = id_without_checksum + checksum
        return id_number
        
        
    def getDrugs(self):
        with open(current_dir+'/data/train.json',mode='r',encoding='utf-8')as f:
            train_json=f.read().split('\n')
        train_json=[eval(i) for i in train_json]
        drugs=[]
        for i in train_json:
            entityMentions=i['entityMentions']
            for entityMention in entityMentions:
                label=entityMention['label']
                if label == 'NDR':
                    drugs.append(entityMention['text'])
        drugs=set(drugs)
        return list(drugs)
    
    def GetId(self,Datasets,IDS,name):
        
        for index,data in enumerate(Datasets):
            if data['new_name'] == name:
                if data['numOfTime'] < data['limit']:
                    data['numOfTime'] += 1
                    return data['ID']
                else:
                    while True:
                        person_id = self.generate_random_id()
                        if person_id not in IDS:
                            Datasets[index]['ID'] = person_id
                            Datasets[index]['numOfTime'] = 1
                            IDS.append(person_id)
                            return person_id
        new_person={}
        while True:
            person_id = self.generate_random_id()
            if person_id not in IDS:
                new_person['new_name'] = name
                new_person['ID'] = person_id
                new_person['numOfTime'] = 1
                new_person['limit'] = 3
                IDS.append(person_id)
                Datasets.append(new_person)
                return person_id
        
    def unique_list(self,entityMentions:list):
        result=[]
        for i in entityMentions:
            if i not in result:
                result.append(i)
        return result        
    
    def getIds(self,entityMentions):
        res={}
        names = set()
        Datasets = pickle.load(open(current_dir+'/data/dataset.pkl','rb'))
        IDS = pickle.load(open(current_dir+'/data/IDS.pkl','rb'))
        for entityMention in entityMentions:
            label = entityMention['label']
            if label != 'Nh':
                continue
            names.add(entityMention['text'])
        
        names = list(names)
        for name in names:
            name_id = self.GetId(Datasets,IDS,name)
            res[name] = name_id
        return res,Datasets,IDS
            
    
    def run(self,text,AID):
        print("start run Main")
        entity = self.entityRecognizer.run(text)[0]
        print("实体识别完成")
        print(entity)
        sentText = entity['sentText']
        entityMentions = entity['entityMentions']
        entityMentions = self.unique_list(entityMentions)
        res={}
        res['articleId'] = AID
        
        name_ids,Datasets,IDS = self.getIds(entityMentions)
        
        for idx,entityMention in enumerate(entityMentions):
            label = entityMention['label']
            if label != 'Nh':
                continue
            entityMentions[idx]['ID'] = name_ids[entityMentions[idx]['text']]
        
        res['entityMentions']=entityMentions
        res['sentText']=sentText
        relationMentions=[]
        
        
        
        for i in range(len(entityMentions)):
            if entityMentions[i]['label']=='NDR' and entityMentions[i]['text'] not in self.drugs:
                continue
            for j in range(i+1,len(entityMentions)):
                if entityMentions[j]['label']=='NDR' and entityMentions[j]['text'] not in self.drugs:
                    continue
                label1=entityMentions[i]['label']
                label2=entityMentions[j]['label']
                if [label1,label2] not in [['Nh','Nh'],['Nh','NDR']]:
                    continue
                pre_relation=self.relation_predictor.predict_relation(entityMentions[i]['text'],entityMentions[j]['text'],sentText)
                if pre_relation == 'NA':
                    continue
                relationMention={}
                relationMention['em1Text']=entityMentions[i]['text']
                relationMention['em2Text']=entityMentions[j]['text']
                relationMention['label']=pre_relation
                relationMentions.append(relationMention)
        res['relationMentions']=relationMentions
        print(res)
        Graph_Interface(res)
        res_neo4j = self.neo4j_query_executor.run(AID)
        return res_neo4j,Datasets,IDS
    
if __name__ == "__main__":
    m = Main()
    print(1)
        