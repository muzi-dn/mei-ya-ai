#coding:utf-8
import os
import json
from py2neo import Graph,Node


temp_dict = {}
temp_dict_all = {}

# 先连接
g = Graph('http://localhost:7474',name = "neo4j",password = "neo4jneo4j")
def interface(msg):

    # 身份ID
    ID =1000
    # 用来判断是否为空
    address = False
    time = False
     # 这里用来判断某个属性是否为空
    check = ['Nh','Ns','NT','NW','NDR']
    # 读取每一个点,需要添加属性的（2个）
    person_infos = []
    drug_infos = []
    # 读取关系（4个）
    sell_drug_to = []
    traffic_in = []
    possess = []
    provide_shelter_for = []
    # 更改关系数据(为关系添加articleId,时间、地点,人的身份ID,毒品重量)，为后面建立图谱做准备
    json_datas = json.loads(msg)
    # print(type(json_datas))
    temp_dict['articleId'] = json_datas['articleId']
    for json_data in json_datas['entityMentions']:
        if json_data['label'] == 'NT':
            temp_dict['time'] = json_data['text']
            time =True
        if json_data['label'] == 'Ns':
            temp_dict['address'] = json_data['text']
            address = True
        if not time:
            temp_dict['time'] = None
        if not address:
            temp_dict['address'] = None
    for json_data in json_datas['relationMentions']:
        for json_en_data in json_datas['entityMentions']:
            if json_en_data['label'] == 'Nh' and json_data['em1Text'] == json_en_data['text']:
                temp_dict['ID1'] = json_en_data['ID']
            if json_en_data['label'] == 'Nh' and json_data['em2Text'] == json_en_data['text']:
                temp_dict['ID2'] = json_en_data['ID']
        if json_data['label'] !='NA':
            json_data.update(temp_dict)
        temp_dict_all = json_data
        json_datas.update(temp_dict_all)
        del json_datas['em2Text']
        del json_datas['label']
        # del json_datas['articleId']
        del json_datas['time']
        del json_datas['em1Text']
        del json_datas['address']
        del json_datas['ID1']
        if 'ID2' in json_datas:
            del json_datas['ID2']
        # print(json_datas)
        # 数据处理完毕，开始
    drug_dict = {}
    
    for json_data in json_datas["entityMentions"]:
        person_dict = {}
        person_dict['articleId'] = json_datas['articleId']
        drug_dict['articleId'] = json_datas['articleId']
        # print(json_data)
        if json_data['label']=='Nh':
            # print(json_data)
            person_dict['name'] = json_data['text']
            person_dict['ID'] = json_data['ID']
            # print(person_dict)
        elif json_data['label']=='NDR':
            drug_dict['name'] =json_data['text']
        elif json_data['label'] == 'NW':
            drug_dict['weight']=json_data['text']
        # print(len(person_dict))
        if(len(person_dict)==3):
            person_infos.append(person_dict)
        # print(len(drug_dict))
    if len(drug_dict)==2:
        if 'weight' not in drug_dict:
            drug_dict['weight'] = None
        elif 'name' not in drug_dict:
            drug_dict['name'] = None
    drug_infos.append(drug_dict)
        
    for json_data in json_datas['relationMentions']:
        if json_data['label']=='traffic_in':
            traffic_in.append([json_data['em1Text'],json_data['em2Text'],str(json_data['articleId']),json_data['ID1'],json_data['time'],str(json_data['address'])])
        if json_data['label']=='sell_drugs_to':
            sell_drug_to.append([json_data['em1Text'],json_data['em2Text'],str(json_data['articleId']),json_data['ID1'],json_data['ID2'],json_data['time'],str(json_data['address'])])
        if json_data['label'] =='posess':
            possess.append([json_data['em1Text'],json_data['em2Text'],str(json_data['articleId']),json_data['ID1'],json_data['time'],str(json_data['address'])])
        if json_data['label']=='provide_shelter_for':
            provide_shelter_for.append([json_data['em1Text'],json_data['em2Text'],str(json_data['articleId']),json_data['ID1'],json_data['ID2'],json_data['time'],str(json_data['address'])])
    # print(person_infos,drug_infos,traffic_in)
    return  person_infos,drug_infos,sell_drug_to,traffic_in,possess,provide_shelter_for
        # 建立人的节点(属性包括姓名和身份ID和文章ID)
def create_person_node(person_infos):
    count = 0
    # print(person_infos)
    for person in person_infos:
        node = Node("Person",ID=person['ID'],name=person['name'],articleId = person['articleId'])
        g.create(node)
        count+=1
        # print(count,len(nodes))
    return     
def create_drug_node(drug_infos):
    count = 0
    for drug in drug_infos:
        node = Node("drug",name = drug['name'],weight = drug['weight'],articleId = drug['articleId'])
        g.create(node)
        count+=1
        # print(count,len(nodes))
    return 
def create_graphnodes(msg):
    person_infos,drug_infos,sell_drug_to,traffic_in,possess,provide_shelter_for = interface(msg)
    create_person_node(person_infos)
    create_drug_node(drug_infos)
    return  
    # 创建关系边
def create_relationship(start_node,end_node,edges,rel_type,rel_name):
    count = 0
    set_edges = []
    for edge in edges:
        set_edges.append("+++".join(edge))
    all = len(set(set_edges))
    for edge in set(set_edges):
        edge = edge.split('+++')   
        # print(edge)
        if rel_type =='traffic_in' or rel_type == 'possess':
            p = edge[0]
            q = edge[1]
            articleId = edge[2]
            ID = edge[3]
            time = edge[4]
            address = edge[5]
            query = "match(p:%s),(q:%s) where p.name='%s' and q.name='%s' and q.articleId =%s and p.ID='%s'  create (p) -[rel:%s{name:'%s',time:'%s',address:\"%s\"}]->(q)"%(start_node,end_node,p,q,articleId,ID,rel_type,rel_name,time,address)
        else:
            p = edge[0]
            q = edge[1]
            articleId = edge[2]
            ID1 = edge[3]
            ID2 = edge[4]
            time = edge[5]
            address = edge[6]
            query = "match(p:%s),(q:%s) where p.name='%s' and q.name ='%s' and p.ID = '%s' and q.ID ='%s' create(p) -[rel:%s{name:'%s',time:'%s',address:\"%s\"}]->(q)"%(start_node,end_node,p,q,ID1,ID2,rel_type,rel_name,time,address)
        try:
            g.run(query)
            count+=1
            # print(rel_type,count,all)
        except Exception as e:
            print(e)
    return 
# 创建图的边
def create_graphrels(msg):
    person_infos,drug_infos,sell_drug_to,traffic_in,possess,provide_shelter_for= interface(msg)
    create_relationship("Person","Person",sell_drug_to,'sell_drug_to','售卖(给人)')
    create_relationship("Person","drug",traffic_in,'traffic_in','售卖(毒品)')
    create_relationship("Person","drug",possess,'possess','非法拥有')
    create_relationship("Person","Person",provide_shelter_for,'provide_shelter_for','提供庇护')      
            
            
            
            
            
            
            
            
            
def Graph_Interface(dict_data):
    json_data = json.dumps(dict_data,ensure_ascii=False)
    interface(json_data)
    create_graphnodes(json_data)
    create_graphrels(json_data)
               
if __name__ == "__main__":
    dict_data ={'articleId': 55, 
'entityMentions': [{'text': '凌某', 'label': 'Nh', 'ID': '646494196304288992'}, {'text': '2014年11月', 'label': 'NT'}, {'text': '海洛因', 'label': 'NDR'}], 
'sentText': '被告人凌某自2014年11月始多次贩卖毒品海洛因给他人,贩卖海洛因共计17.1298克', 
'relationMentions': [{'em1Text': '凌某', 'em2Text': '海洛因', 'label': 'traffic_in'}]}
    Graph_Interface(dict_data)