# -*- coding: utf-8 -*-
from py2neo import Graph


class Neo4jQueryExecutor_name:
    def __init__(self, url='http://localhost:7474', username='neo4j', password='neo4jneo4j'):
        self.graph = Graph(url, auth=(username, password))
        self.result = {'type': 'force'}

        self.categories = [
            {
                "name": "贩毒(给人)",
                "itemStyle": {
                    "color": "#cb4335"
                }
            },
            {
                "name": "贩卖(毒品)",
                "itemStyle": {
                    "color": "#f1c40f"
                }
            },
            {
                "name": "持有",
                "itemStyle": {
                    "color": "#3498db"
                }
            },
            {
                "name": "非法容留",
                "itemStyle": {
                    "color": "#229954"
                }
            },
            {
                "name": "Person",
                "itemStyle": {
                    "color": "#b3b6b7"
                }
            },
            {
                "name": "Drug",
                "itemStyle": {
                    "color": "#283747"
                }
            }
        ]
        self.result['categories'] = self.categories
        self.mapTable = {
            "sell_drug_to": '贩毒(给人)',
            'traffic_in': '贩卖(毒品)',
            "possess": '持有',
            "provide_shelter_for": "非法容留"

        }
        self.nodes = []
        self.types = ['售卖(给人)', '售卖(毒品)', '非法拥有', '提供庇护']
        self.links = []

    def check(self, node):
        for n in self.nodes:
            if n == node:
                return True
        return False

    def getNodes(self, name):
        cypher_query = f"""
            MATCH (start_node)-[r]->(end_node)
            WHERE start_node.name = "{name}" OR end_node.name = "{name}"
            RETURN r;

            """
        result = self.graph.run(cypher_query)
        nodes = []
        for recode in result:
            relationships = recode["r"]

            nodess = [relationships.start_node, relationships.end_node]
            for node in nodess:
                tmp = {}
                values = {}
                node_dict = dict(node)
                node_dict.pop('sentId', 0)
                node_dict.pop("articleId", 0)
                if self.check(str(node_dict)):
                    continue

                self.nodes.append(str(node_dict))
                label = str(node.labels)[1:]
                node_properties = dict(node)

                category = self.getCategories(relationships=relationships)
                values["category"] = category
                for key, value in node_properties.items():
                    if key in ["name"]:
                        continue
                    if key == 'articleId':
                        key = 'AID'
                    values[key] = value

                tmp['name'] = node_properties['name']
                tmp['value'] = values

                if label == 'Person':
                    tmp['category'] = 4
                else:
                    tmp['category'] = 5

                nodes.append(tmp)
        self.result['nodes'] = nodes

    def run(self, name="黄凤英"):
        self.getNodes(name=name)
        self.getRelationships(name=name)
        return self.result

    def FindCategories(self, type_):
        for index, category in enumerate(self.categories):
            if category["name"] == type_:
                return index
        return None

    def getCategories(self, relationships):
        result = []
        for relationship in relationships:
            type_ = str(type(relationships)).split('\'')[1].split('.')[-1]
            type_ = self.mapTable[type_]
            index = self.FindCategories(type_)
            if index is not None:
                result.append(index)
        return list(set(result))

    def getRelationships(self, name):
        cypher_query = f"""
        MATCH (start_node)-[r]->(end_node)
        WHERE start_node.name = "{name}" OR end_node.name = "{name}"
        RETURN r;
        """
        rem_links = []
        result = self.graph.run(cypher_query)
        index = 0

        rem = {}  # 查看两个节点是否已经建立关系
        for record in result:
            tmp = {}
            values = {}
            lineStyle = {}
            relationship = record["r"]
            # 获取关系的起始节点
            start_node = relationship.start_node

            # 获取关系的结束节点
            end_node = relationship.end_node

            start_node_dict = dict(start_node)
            start_node_dict.pop('sentId', 0)
            start_node_dict.pop("articleId", 0)
            if not self.check(str(start_node_dict)):
                continue

            end_node_dict = dict(end_node)
            end_node_dict.pop('sentId', 0)
            end_node_dict.pop("articleId", 0)

            if not self.check(str(end_node_dict)):
                continue

            target = self.nodes.index(str(start_node_dict))
            source = self.nodes.index(str(end_node_dict))
            target, source = source, target
            # 获取关系的属性
            relationship_properties = dict(relationship)
            tmp['id'] = index
            tmp['source'] = source
            tmp['target'] = target
            values['type'] = self.types.index(relationship_properties['name'])
            values['time'] = relationship_properties['time']
            address = relationship_properties['address']
            values['place'] = address if address != 'None' else ''

            lineStyle['color'] = self.getColor(values['type'])

            if (source, target) in rem:
                rem[(source, target)] += 0.2
                rem[(source, target)] = round(rem[(source, target)], 1)
                lineStyle['curveness'] = rem[(source, target)]
                rem[(target, source)] = rem[(source, target)]

            elif (target, source) in rem:
                rem[(target, source)] += 0.2
                rem[(target, source)] = round(rem[(target, source)], 1)
                lineStyle['curveness'] = rem[(target, source)]
                rem[(source, target)] = rem[(target, source)]
            else:
                rem[(target, source)] = 0.0
                rem[(source, target)] = 0.0

            index += 1
            tmp['value'] = values
            tmp['lineStyle'] = lineStyle
            tmpp = tmp.copy()
            if not self.check_links(tmpp, rem_links):
                self.links.append(tmp)
                rem_links.append(tmpp)
        self.result['links'] = self.links

    def check_links(self, tmpp, rem_links):
        for link in rem_links:
            if link['source'] == tmpp['source'] and link['target'] == tmpp['target'] and link['value']['type'] == \
                    tmpp['value']['type'] and link['value']['time'] == tmpp['value']['time'] and link['value'][
                'place'] == tmpp['value']['place']:
                return True

        return False

    def getColor(self, index):
        return self.categories[index]['itemStyle']['color']


if __name__ == '__main__':
    t = Neo4jQueryExecutor_name()
    t.run(name="黄凤英")
