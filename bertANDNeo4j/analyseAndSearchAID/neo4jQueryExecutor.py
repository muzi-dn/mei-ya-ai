# -*- coding: utf-8 -*-
import json
import py2neo
from py2neo import Graph
import tqdm


class Neo4jQueryExecutor:
    def __init__(self, url='http://localhost:7474', username='neo4j', password='neo4jneo4j'):
        self.graph = Graph(url, auth=(username, password))
        self.result = {'type': 'force'}

        self.categories = [
            {
                "name": "贩毒(给人)",
                "itemStyle": {
                    "color": "#cb4335"
                }
            },
            {
                "name": "贩卖(毒品)",
                "itemStyle": {
                    "color": "#f1c40f"
                }
            },
            {
                "name": "持有",
                "itemStyle": {
                    "color": "#3498db"
                }
            },
            {
                "name": "非法容留",
                "itemStyle": {
                    "color": "#229954"
                }
            },
            {
                "name": "Person",
                "itemStyle": {
                    "color": "#b3b6b7"
                }
            },
            {
                "name": "Drug",
                "itemStyle": {
                    "color": "#283747"
                }
            }
        ]
        self.result['categories'] = self.categories
        self.mapTable = {
            "sell_drug_to": '贩毒(给人)',
            'traffic_in': '贩卖(毒品)',
            "possess": '持有',
            "provide_shelter_for": "非法容留"

        }
        self.nodes = []
        # self.types = ['售卖(毒品)', '售卖(给人)', '非法拥有', '提供庇护']
        self.types = [ '售卖(给人)', '售卖(毒品)', '非法拥有', '提供庇护']
        self.links = []

    def FindCategories(self, type_):
        for index, category in enumerate(self.categories):
            if category["name"] == type_:
                return index
        return None

    def getCategories(self, relationships):
        result = []
        for relationship in relationships:
            type_ = str(type(relationship)).split(".")[-1].split("'")[0]
            type_ = self.mapTable[type_]
            index = self.FindCategories(type_)
            if index is not None:
                result.append(index)
        return list(set(result))

    
    def getNodes(self, articleId=21):
        # 编写查询语句
        cypher_query = f"""
    MATCH (n)
    WHERE n.articleId = {articleId}
    OPTIONAL MATCH (n)-[r]-()
    RETURN n, labels(n) AS labels, COLLECT(r) AS relationships
    """
        result = self.graph.run(cypher_query)
        nodes = []
        for recode in result:
            tmp = {}
            values = {}
            node = recode['n']
            self.nodes.append(node)
            label = recode["labels"][0]
            node_properties = dict(node)
            relationships = recode["relationships"]
            category = self.getCategories(relationships=relationships)
            values["category"] = category
            for key, value in node_properties.items():
                if key == "name":
                    continue
                if key == 'articleId':
                    key = 'AID'
                values[key] = value
            tmp['name'] = node_properties['name']
            tmp['value'] = values

            if label == 'Person':
                tmp['category'] = 4
            else:
                tmp['category'] = 5
            nodes.append(tmp)
        self.result['nodes'] = nodes

    def getColor(self, index):
        return self.categories[index]['itemStyle']['color']

    def getRelationships(self, articleId=21):
        cypher_query = f"""
        MATCH (start_node)-[r]->(end_node)
        WHERE start_node.articleId = {articleId} OR end_node.articleId = {articleId}
        RETURN r;
        """
        result = self.graph.run(cypher_query)
        index = 0
        rem = {}  # 查看两个节点是否已经建立关系
        for record in result:
            tmp = {}
            values = {}
            lineStyle = {}
            relationship = record["r"]
            
            
            # 获取关系的起始节点
            start_node = relationship.start_node
            
            if start_node not in self.nodes:
                continue
            
            source = self.nodes.index(start_node)

            # 获取关系的结束节点
            end_node = relationship.end_node
            
            if end_node not in self.nodes:
                continue
            
            target = self.nodes.index(end_node)

            # 获取关系的属性
            relationship_properties = dict(relationship)

            tmp['id'] = index
            tmp['source'] = source
            tmp['target'] = target

            values['type'] = self.types.index(relationship_properties['name'])
            values['time'] = relationship_properties['time']
            address = relationship_properties['address']
            values['place'] = address if address != 'None' else ''

            lineStyle['color'] = self.getColor(values['type'])

            if (source, target) in rem:
                rem[(source, target)] += 0.2
                rem[(source, target)] = round(rem[(source, target)], 1)
                lineStyle['curveness'] = rem[(source, target)]
                rem[(target, source)] = rem[(source, target)]

            elif (target, source) in rem:
                rem[(target, source)] += 0.2
                rem[(target, source)] = round(rem[(target, source)], 1)
                lineStyle['curveness'] = rem[(target, source)]
                rem[(source, target)] = rem[(target, source)]
            else:
                rem[(target, source)] = 0.0
                rem[(source, target)] = 0.0

            index += 1
            tmp['value'] = values
            tmp['lineStyle'] = lineStyle
            self.links.append(tmp)
        self.result['links'] = self.links

    def run(self, articleId=21):
        self.getNodes(articleId=articleId)
        self.getRelationships(articleId=articleId)
        return self.result


if __name__ == '__main__':
    t = Neo4jQueryExecutor()
    t.run(articleId=21)
