import re
def find_entity():
    question = input("Please input a question：")
    # 八种问题的问法。
    entity_num =0
    entity = ''
    pattern1 = r"(.*?)贩卖过几次毒品？"
    pattern2 = r"(.*?)被哪些人贩卖过？"
    pattern3 = r"(.*?)非法容留过谁？"
    pattern4 = r"(.*?)将毒品出售给过谁？"
    pattern5 = r"(.*?)非法拥有过哪些毒品"
    pattern6 = r"(.*?)和哪些人有关系？关系是什么？"
    pattern7 = r"在(.*?)发生过哪些关于毒品的违法犯罪行为？"
    pattern8 = r"发生在(.*?)的毒品违法犯罪有哪些？"
    match1 = re.search(pattern1,question)
    match2 = re.search(pattern2,question)
    match3 = re.search(pattern3,question)
    match4 = re.search(pattern4,question)
    match5 = re.search(pattern5,question)
    match6 = re.search(pattern6,question)
    match7 = re.search(pattern7,question)
    match8 = re.search(pattern8,question)
    if match1:
        entity = match1.group(1)
        entity_num =1
    elif match2:
        entity = match2.group(1)
        entity_num =2
    elif match3:
        entity = match3.group(1)
        entity_num =3
    elif match4:
        entity = match4.group(1)
        entity_num =4
    elif match5:
        entity = match5.group(1)
        entity_num =5
    elif match6:
        entity = match6.group(1)
        entity_num =6
    elif match7:
        entity = match7.group(1)
        entity_num =7
    elif match8:
        entity = match8.group(1)
        entity_num =8
    return entity,entity_num
if __name__ =="__main__":
        entity = find_entity()