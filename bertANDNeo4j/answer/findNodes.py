# 1、固定句式，固定关键词位置，查找对应的实体关键词
# 2、去数据库查询相关信息，并返回
from find_entity import *
from py2neo import Graph
def read_nodes():
    graph = Graph("http://localhost:7474",name="neo4j",password="neo4jneo4j")
    entity,entity_num= find_entity()
    if entity =='':
        return "今夕几何？没有犯罪记录哟"
    sqls = {}
    # 总共八个问题
    sqls['cypher_1'] ="match(m:Person)-[r:traffic_in]->(n:drug) where m.name='{0}' return m.name,r.name,n.weight,n.name".format(entity)
    # print(sqls['cypher_1'])
    sqls['cypher_2'] ="match(m:Person)-[r:traffic_in]->(n:drug) where n.name='{0}' return m.name,r.name,n.name".format(entity)
    sqls['cypher_3'] ="match(m:Person)-[r:provide_shelter_for]->(n:Person) where m.name='{0}' return m.name,r.name,n.name".format(entity)
    sqls['cypher_4'] ="match(m:Person)-[r:sell_drug_to]->(n:Person) where m.name='{0}' return m.name,r.name,n.name".format(entity)
    sqls['cypher_5'] ="match(m:Person)-[r:possess]->(n:drug) where m.name='{0}' return m.name,n.name,n.weight".format(entity)
    sqls['cypher_6'] ="match(m:Person)-[r]->(n:Person) where m.name='{0}' return m.name,r.name,n.name".format(entity) 
    sqls['cypher_7'] ="match(m)-[r]->(n) where r.address='{0}' return n.name,r.name,r.address,m.name".format(entity)
    sqls['cypher_8'] ="match(m)-[r]->(n) where r.time ='{0}'return n.name,r.name,r.time,m.name".format(entity)
    sql = "cypher_"+str(entity_num)
    # print(sqls[sql])
    answers = graph.run(sqls[sql]).data()
    if not answers:
        return "今夕几何？没有犯罪记录哟"
    # print(answers)
    # 分别对应最后的输出
    if sql == 'cypher_1':
        new_answers = [dict(d) for d in(set([tuple(d.items())for d in answers]))]
        desc = [i['n.name'] for i in new_answers]
        desc_weight = [i['n.weight'] for i in new_answers]
        subject = answers[0]['m.name']
        final_answer='*****贩毒记录如下*****\n'
        t = dict(zip(desc,desc_weight))
        answer_num=0
        for p,w in t.items():
            answer_num+=1
            answer = '{0}、{1}非法售卖{2}{3}\n'.format(answer_num,subject,p,w)
            final_answer+=answer
        final_answer+='\n{0}非法售卖毒品总计{1}次'.format(subject,answer_num)
    elif sql == 'cypher_2':
        desc = [i['m.name']for i in answers]
        subject = answers[0]['n.name']
        final_answer = '贩卖过{0}的人有：{1}'.format(subject,';'.join(list(set(desc))))
        final_answer+='总计{0}人'.format(len(set(desc)))
    elif sql == 'cypher_3':
        desc = [i['n.name'] for i in answers]
        subject = answers[0]['m.name']
        final_answer = '{0}非法容留过以下的人：{1}'.format(subject,';'.join(list(set(desc))))
        final_answer+='总计{0}人'.format(len(set(desc)))
    elif sql =='cypher_4':
        desc = [i['n.name'] for i in answers]
        subject = answers[0]['m.name']
        final_answer = '{0}将毒品贩卖给过以下人员：{1}'.format(subject,';'.join(list(set(desc))))
        final_answer +='总计{0}人'.format(len(desc))
    elif sql =='cypher_5':
        new_answers = [dict(d) for d in(set([tuple(d.items())for d in answers]))]
        desc = [i['n.name'] for i in new_answers]
        desc_weight = [i['n.weight'] for i in new_answers]
        subject = answers[0]['m.name']
        final_answer=''
        t = dict(zip(desc,desc_weight))
        answer_num=0
        for p,w in t.items():
            answer_num+=1
            answer = '{0}、{1}非法拥有{2}{3}\n'.format(answer_num,subject,p,w)
            final_answer+=answer
        final_answer+='{0}非法拥有毒品总计{1}次'.format(subject,answer_num)
    elif sql =='cypher_6':
        new_answers = [dict(d) for d in(set([tuple(d.items())for d in answers]))]
        desc = [i['n.name'] for i in new_answers]
        desc_relation = [i['r.name'] for i in new_answers]
        # print(new_answers)
        subject = answers[0]['m.name']
        final_answer='*****关系如下*****\n'
        t = dict(zip(desc,desc_relation))
        answernum=0
        for p,r in t.items():
            answernum+=1
            if r=='售卖(给人)':
                answer = '{0}、{1}将毒品售卖给{2}\n'.format(answernum,subject,p)
            elif r =='提供庇护':
                answer = '{0}、{1}为{2}吸食毒品提供庇护\n'.format(answernum,subject,p)
            final_answer+=answer
        final_answer+='和{0}有关系的人总计{1}位'.format(subject,answernum)
    elif sql =='cypher_7':
        new_answers = [dict(d) for d in(set([tuple(d.items())for d in answers]))]
        desc_e1 = [i['m.name'] for i in new_answers]
        desc_r = [i['r.name'] for i in new_answers]
        desc_e2 = [i['n.name'] for i in new_answers]
        final_answer = '*****发生过的案例如下*****\n'
        answer_num = 0
        subject = answers[0]['r.address']
        for e1,r,e2 in zip(desc_e1,desc_r,desc_e2):
            answer_num+=1
            if r =='售卖(给人)':
                answer = '{0}、{1}在{2}售卖毒品给{3}\n'.format(answer_num,e1,subject,e2)  
            elif r == '提供庇护':
                answer = '{0}、{1}在{3}非法容留{2}吸食毒品\n'.format(answer_num,e1,e2,subject)
            elif r =='售卖(毒品)':
                answer = '{0}、{1}在{2}售卖{3}\n'.format(answer_num,e1,subject,e2)
            elif r == '非法拥有':
                answer ='{0}、{1}在{3}私藏{2}这种毒品\n'.format(answer_num,e1,e2,subject)
            final_answer+=answer
        final_answer+='在{0}总共发生过{1}例案件'.format(subject,answer_num)
    elif sql =='cypher_8':
        new_answers = [dict(d) for d in(set([tuple(d.items())for d in answers]))]
        desc_e1 = [i['m.name'] for i in new_answers]
        desc_r = [i['r.name'] for i in new_answers]
        desc_e2 = [i['n.name'] for i in new_answers]
        final_answer = '*****发生在这个时间的有*****'
        answer_num = 0
        subject = answers[0]['r.time']
        for e1,r,e2 in zip(desc_e1,desc_r,desc_e2):
            answer_num+=1
            if r =='售卖(给人)':
                answer = '{0}、{1}在{2}售卖毒品给{3}\n'.format(answer_num,e1,subject,e2)  
            elif r == '提供庇护':
                answer = '{0}、{1}在{3}非法容留{2}吸食毒品\n'.format(answer_num,e1,e2,subject)
            elif r =='售卖(毒品)':
                answer = '{0}、{1}在{2}售卖{3}\n'.format(answer_num,e1,subject,e2)
            elif r == '非法拥有':
                answer ='{0}、{1}在{3}私藏{2}这种毒品\n'.format(answer_num,e1,e2,subject)
            final_answer+=answer
        final_answer+='在{0}总共发生过{1}例案件'.format(subject,answer_num)
    return final_answer
if __name__ =="__main__":
    answer = read_nodes()
    print(answer)