import json

# 打开 JSON 文件
res = []
with open('/root/bertANDneo4j/createneo4j/new_train.json', 'r') as f:
    # 读取 JSON 数据
    data = f.read().split("\n")
    data = [eval(i) for i in data if len(i) >0]

    # 处理 JSON 数据
    for d in data:
        tmp_res = {}
        tmp_res["articleId"] = d['articleId']
        tmp_res["sentId"] = d['sentId']
        tmp_res["entityMentions"] = d['entityMentions']
        tmp_res["sentText"] = d['sentText']
        tmp_res["relationMentions"] = d['relationMentions']
        res.append(tmp_res)

# print(res)
with open('data.json', 'a') as f:
    for r in res:
        json.dump(r, f, separators=(',', ':'), ensure_ascii=False)
        f.write("\n")