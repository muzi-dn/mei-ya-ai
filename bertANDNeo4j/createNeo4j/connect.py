import os
import json
from py2neo import Graph,Node
import io
import ast
from tqdm import tqdm
temp_dict = {}
temp_dict_all = {}

# 先连接
g = Graph('http://localhost:7474',name = "neo4j",password = "neo4jneo4j")
def interface(msg):

    # 用来判断是否为空
    address = False
    time = False
     # 这里用来判断某个属性是否为空
    check = ['Nh','Ns','NT','NW','NDR']
    # 读取每一个点,需要添加属性的（2个）
    person_infos = []
    drug_infos = []
    # 读取关系（4个）
    sell_drug_to = []
    traffic_in = []
    possess = []
    provide_shelter_for = []
    # 更改关系数据(为关系添加articleId,时间、地点,人的身份ID,毒品重量)，为后面建立图谱做准备
    json_datas = json.loads(msg)
    # print(type(msg))
    # print(json_datas)
    # print(type(json_datas))
    temp_dict['articleId'] = json_datas['articleId']
    for json_data in json_datas['entityMentions']:
        if json_data['label'] == 'NT':
            temp_dict['time'] = json_data['text']
            time =True
        if json_data['label'] == 'Ns':
            temp_dict['address'] = json_data['text']
            address = True
        if not time:
            temp_dict['time'] = "无数据提供"
        if not address:
            temp_dict['address'] = "无数据提供"
    for json_data in json_datas['relationMentions']:
        for json_en_data in json_datas['entityMentions']:
            if json_en_data['label'] == 'Nh' and json_data['em1Text'] == json_en_data['text']:
                temp_dict['ID1'] = json_en_data['id']
            if json_en_data['label'] == 'Nh' and json_data['em2Text'] == json_en_data['text']:
                temp_dict['ID2'] = json_en_data['id']
        if json_data['label'] !='NA':
            json_data.update(temp_dict)
        temp_dict_all = json_data
        json_datas.update(temp_dict_all)
        if 'em2Text' in json_datas:
            del json_datas['em2Text']
        if 'label' in json_datas:
            del json_datas['label']
        # del json_datas['articleId']
        if 'time' in json_datas:
            del json_datas['time']
        if 'em1Text' in json_datas:
            del json_datas['em1Text']
        if 'address' in json_datas:
            del json_datas['address']
        if 'ID1' in json_datas:
            del json_datas['ID1']
        if 'ID2' in json_datas:
            del json_datas['ID2']
        # print(json_datas)
        # 数据处理完毕，开始
    drug_dict = {}
    
    for json_data in json_datas["entityMentions"]:
        person_dict = {}
        person_dict['articleId'] = json_datas['articleId']
        person_dict['sentId'] = json_datas['sentId']
        drug_dict['articleId'] = json_datas['articleId']
        drug_dict['sentId'] = json_datas['sentId']
        # print(json_data)
        if json_data['label']=='Nh':
            # print(json_data)
            person_dict['name'] = json_data['text']
            person_dict['ID'] = json_data['id']
            # print(person_dict)
        elif json_data['label']=='NDR':
            drug_dict['name'] =json_data['text']
        elif json_data['label'] == 'NW':
            drug_dict['weight']=json_data['text']
        # print(len(person_dict))
        # 文章ID,段落ID，姓名，身份证ID(缺一不可，所以不需要另外确认)
        if(len(person_dict)==4):
            person_infos.append(person_dict)
        # print(len(drug_dict))
        # 文章ID,段落ID,毒品名称，重量
        if(len(drug_dict)==4):
            drug_infos.append(drug_dict)
            drug_dict = {}
    if len(drug_dict)==3:
        if 'weight' not in drug_dict:
            drug_dict['weight'] = "无数据提供"
        elif 'name' not in drug_dict:
            drug_dict['name'] = "毒品"
        drug_infos.append(drug_dict)
        
    for json_data in json_datas['relationMentions']:
        # print(json_data)
        if json_data['label']=='traffic_in':
            traffic_in.append([json_data['em1Text'],json_data['em2Text'],str(json_data['articleId']),json_data['ID1'],json_data['time'],str(json_data['address'])])
        if json_data['label']=='sell_drugs_to':
            sell_drug_to.append([json_data['em1Text'],json_data['em2Text'],str(json_data['articleId']),json_data['ID1'],json_data['ID2'],json_data['time'],str(json_data['address'])])
        if json_data['label'] =='posess':
            possess.append([json_data['em1Text'],json_data['em2Text'],str(json_data['articleId']),json_data['ID1'],json_data['time'],str(json_data['address'])])
        if json_data['label']=='provide_shelter_for':
            provide_shelter_for.append([json_data['em1Text'],json_data['em2Text'],str(json_data['articleId']),json_data['ID1'],json_data['ID2'],json_data['time'],str(json_data['address'])])
    # print(type(person_infos))
    # print(person_infos)
    return  person_infos,drug_infos,sell_drug_to,traffic_in,possess,provide_shelter_for
        # 建立人的节点(属性包括姓名和身份ID和文章ID)
def create_person_node(person_infos):
    count = 0
    # print(type(person_infos))
    # print(person_infos)
    person_infos1 = set(tuple(sorted(d.items()))for d in person_infos)
    person_infos = [dict(t) for t in person_infos1]
    # print(person_infos)
    for person in person_infos:
        node = Node("Person",ID=person['ID'],name=person['name'],articleId = person['articleId'],sentId = person['sentId'])
        g.create(node)
        count+=1
        # print(count,len(nodes))
    return     
def create_drug_node(drug_infos):
    count = 0
    for drug in drug_infos:
        node = Node("drug",name = drug['name'],weight = drug['weight'],articleId = drug['articleId'],sentId = drug['sentId'])
        g.create(node)
        count+=1
        # print(count,len(nodes))
    return 
def create_graphnodes(msg):
    person_infos,drug_infos,sell_drug_to,traffic_in,possess,provide_shelter_for = interface(msg)
    create_person_node(person_infos)
    create_drug_node(drug_infos)
    return  
    # 创建关系边
def create_relationship(start_node,end_node,edges,rel_type,rel_name):
    count = 0
    set_edges = []
    for edge in edges:
        # print(edge)
        set_edges.append("+++".join(edge))
    all = len(set(set_edges))
    for edge in set(set_edges):
        edge = edge.split('+++')   
        # print(edge)
        if rel_type =='traffic_in' or rel_type == 'possess':
            p = edge[0]
            p = p.replace("'","")
            q = edge[1]
            q = q.replace("'","")
            articleId = edge[2]
            ID = edge[3]
            time = edge[4]
            time =time.replace("'","")
            address = edge[5]
            address = address.replace("'",'"')
            query = "match(p:%s),(q:%s) where p.name='%s' and q.name='%s' and q.articleId =%s and p.ID='%s'  create (p) -[rel:%s{name:'%s',time:'%s',address:'%s'}]->(q)"%(start_node,end_node,p,q,articleId,ID,rel_type,rel_name,time,address)
            # print(query)
        else:
            p = edge[0]
            p = p.replace("'","")
            q = edge[1]
            q = q.replace("'","")
            articleId = edge[2]
            ID1 = edge[3]
            ID2 = edge[4]
            time = edge[5]
            time =time.replace("'","")
            address = edge[6]
            # print(type(address))
            address = address.replace("'",'"')
            query = "match(p:%s),(q:%s) where p.name='%s' and q.name ='%s' and p.ID = '%s' and q.ID ='%s' create(p) -[rel:%s{name:'%s',time:'%s',address:'%s'}]->(q)"%(start_node,end_node,p,q,ID1,ID2,rel_type,rel_name,time,address)
            # print(query)
        try:
            g.run(query)
            count+=1
            # print(rel_type,count,all)
        except Exception as e:
            print(e)
    return 
# 创建图的边
def create_graphrels(msg):
    person_infos,drug_infos,sell_drug_to,traffic_in,possess,provide_shelter_for= interface(msg)
    create_relationship("Person","Person",sell_drug_to,'sell_drug_to','售卖(给人)')
    create_relationship("Person","drug",traffic_in,'traffic_in','售卖(毒品)')
    create_relationship("Person","drug",possess,'possess','非法拥有')
    create_relationship("Person","Person",provide_shelter_for,'provide_shelter_for','提供庇护')      
            
            
            
def Graph_Interface(dict_data):
    # print(type(dict_data))
    json_data = json.dumps(dict_data)
    interface(json_data)
    create_graphnodes(json_data)
    create_graphrels(json_data)
               
if __name__ == "__main__":
    open_files = ["new_train.json"]
    for file in open_files:
        file_length = 500
        with open(file, 'r', encoding='utf-8') as fps:
            file_length = sum(1 for line in fps)
        with open(file,'r',encoding='utf-8') as fps:
            pbar = tqdm(total=file_length)
            for fp in fps:
                pbar.update(1)
                json_data = eval(fp)
                Graph_Interface(json_data)
            pbar.close()
    