from flask import Flask, request
import requests
from flask_cors import CORS
import json
import time
import pickle
from analyse_and_searchAID.neo4jQueryExecutor import Neo4jQueryExecutor
from analyse_and_searchAID.neo4jQueryExecutor_name import Neo4jQueryExecutor_name
from analyse_and_searchAID.MAIN import Main
from answer.chatbot_graph import ChatBotGraph
from py2neo import Graph

graph = Graph('http://localhost:7474', auth=('neo4j', 'neo4jneo4j'))
new_aid = 2041
with open("/root/bertANDneo4j/analyse_and_searchAID/data/aidmax.pkl", "rb") as f:
    new_aid = pickle.load(f)
print(f"new aid = {new_aid}")

app = Flask(__name__)
CORS(app, origins='*')

Datasets = None
IDS = None

@app.route('/search_aid', methods=['POST'])
def search_aid():
    data = request.json
    input_number = data.get('input')
    neoj = Neo4jQueryExecutor()
    res = neoj.run(input_number)
    # print(res['nodes'])
    # for i in res["links"]:
    #     print(i)
    return res

@app.route('/search_name', methods=['POST'])
def search_name():
    data = request.json
    input_number = data.get('input')
    neoj = Neo4jQueryExecutor_name()
    res = neoj.run(input_number)
    # print(res)
    # for i in res["links"]:
    #     print(i)
    return res

@app.route('/answer', methods=['POST'])
def answer():
    data = request.json
    input_str = data.get('input')
    ans = ChatBotGraph()
    res = ans.chat_main(input_str)
    return res

@app.route('/analyse', methods=['POST'])
def analyse():
    data = request.json
    input_str = data.get('input')
    # print(input_str)
    neoj = Main()
    res,Datasets,IDS = neoj.run(input_str, new_aid)
    # print(res)
    return res

@app.route('/saveORnot', methods=['POST'])
def delete():
    data = request.json
    input_str = data.get('input')
    # print(input_str)
    # 更新pkl
    if input_str == True:
        save_data()
    # 根据AID删除图数据库并更新pkl
    else:
        deletebyAID(new_aid)
    
    return 'delete'
def save_data():
    with open('/root/bertANDneo4j/analyse_and_searchAID/data/dataset.pkl','wb')as f:
        pickle.dump(Datasets,f)
    with open('/root/bertANDneo4j/analyse_and_searchAID/data/IDS.pkl','wb')as f:
        pickle.dump(IDS, f)
    global new_aid
    new_aid = new_aid + 1
    with open("/root/bertANDneo4j/analyse_and_searchAID/data/aidmax.pkl", "wb") as f:
        pickle.dump(new_aid, f)
def deletebyAID(aid):
    cypher_query = f"""
    match(n{{articleId:{aid}}}) DETACH DELETE n;
    """
    # print(cypher_query)
    graph.run(cypher_query)

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=8080)
    # requests.get('http://127.0.0.1:8081')